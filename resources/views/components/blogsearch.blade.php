{!! Form::open(['route' => 'admin.blog.index', 'method' => 'GET']) !!}
<div class="input-group">
    {!! Form::text('name', null,['class' => 'form-control', 'placeholder' =>'Ingresa tu criterio de busqueda...']) !!}
    <span class="input-group-btn">
          {!! Form::submit('Buscar!', ['class' => 'btn btn-warning']) !!}
      </span>
</div>
{!! Form::close() !!}
<br>