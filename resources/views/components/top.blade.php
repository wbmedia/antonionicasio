<div class="jumbotron">

    <div class="container">
        <div class="row">
            <h1>Welcome</h1>

            <h2><i class="glyphicon glyphicon-user"></i>
                <button class="btn btn-success">
                   {{ Auth::user()->username }}
                </button>
            </h2>
            <p>Administracion de el Sistema.</p>

        </div>
    </div>
    @if(Auth::user())
    @include('admin.users.components.navbar-admin')
    @endif


</div>

