@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')

@stop

@section('messages')
    @include('flash::message')

@stop

@section('buscar')
    @include('components.blogsearch')
@stop

@section('contenido')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('admin/blog/create')}}" class="btn btn-info">Crear Entrada</a>
                <hr>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Articulos
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <tr>
                                <th>#</th>
                                <th>Titulo de El Articulo</th>
                                <th>Extracto de el Articulo</th>
                                <th>Categoria</th>
                                <th>Creado Por</th>
                                <th>Acciones</th>

                            </tr>
                            @foreach($blog as $article)
                            <tr>
                                <td>{{$article->id}} </td>
                                <td>{{$article->title}}</td>
                                <td>{{$article->content}}</td>
                                <td><div class="btn btn-primary">{{$article->category->name}}</div></td>
                                <td><div class="btn btn-primary">{{$article->user->username}}</div></td>
                                <td>
                                    <a class="btn btn-warning" href=""><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a class="btn btn-warning" href=""><i class="
glyphicon glyphicon-edit"></i></a>
                                    <a class="btn btn-warning" href=""><i class="glyphicon glyphicon-erase"></i></a>

                                </td>
                            </tr>
                                @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop

@section('scripts')
    @include('components.scripts')
@stop