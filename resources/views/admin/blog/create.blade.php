@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop



@section('messages')
    @include('flash::message')
    <div class="alert alert-danger" role="alert">

        @include('components.errors')
    </div>

@stop

@section('contenido')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('admin/blog')}}" class="btn btn-info">Volver</a>
                <hr>

                {!! Form::open(['route' => 'admin.blog.store', 'method' => 'POST', 'files' => true ]) !!}

                <div class="form-group">
                    {!! Form::label('title', 'Titulo') !!}
                    {!! Form::text('title', null, array(
                    'class' => 'form-control',
                    'placeholder' => 'Ingresa el Titulo'
                    )) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('category_id', 'Categorias') !!}
                    {!! Form::select('category_id',$categories, null, ['placeholder' => 'Selecciona Categoria...', 'class' => 'form-control cat-select', 'required'])!!}

                </div>

                <div class="form-group">
                    {!! Form::label('tags', 'Tags') !!}
                    {!! Form::select('tags[]',$tags, null, [ 'class' => 'form-control tag-select','multiple', 'required'])!!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Cargar Imagen') !!}
                    {!! Form::file('image', array('class' => 'btn btn-warning cargar-imagen', )) !!}

                </div>

                <div class="form-group">
                    {!! Form::label('content', 'Contenido') !!}
                    {!! Form::textarea('content', null,array(
                    'class' => 'form-control blog-editor',

                    )) !!}
                </div>

                <div class="form-group">
                    <button class="btn btn-success btn-lg" type="submit">Crear Noticia</button>
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>

@stop

@section('scripts')
    @include('components.scripts')
@stop

@section('js')


    <script>
        $('.tag-select').chosen({
            placeholder_text_multiple: 'Selecciona un maximo de 5 Tags',
            max_selected_options:'5',
            no_results_text: "No existen criterios de busqueda para tu seleccion",
        })

        $('.blog-editor').trumbowyg();
    </script>


@stop