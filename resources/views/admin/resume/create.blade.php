@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop

@section('message')
    @include('components.errors')
@stop

@section('search')

@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Anadir Curriculum Vitae</h1>
                <div class="panel panel-info">
                    <div class="panel-heading"> Mis Datos</div>
                    <div class="panel-body">
                        @include('admin.resume.components.form')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop





@section('scripts')
    @include('components.scripts')
@stop
