@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
@include('components.top')
@stop

@section('message')
    @include('components.errors')
@stop

@section('search')

@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Mi Curriculum Vitae</h1>
                <div class="panel panel-info">
                    <div class="panel-heading"> Mis Datos</div>
                    <div class="panel-body">
                        <h2>Info Aqui</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop





@section('scripts')
    @include('components.scripts')
@stop
