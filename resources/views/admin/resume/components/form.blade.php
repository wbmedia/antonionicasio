{!! Form::open() !!}

<div class="form-group">
    {!! Form::label('position', 'Posicion') !!}
    {!! Form::text('position', null, ['class' => 'form-control', 'placeholder' => 'Escribe tu posicion ej: Software Developer']) !!}
</div>

<div class="form-group">
    {!! Form::label('extract', 'Breve Resumen') !!}
    {!! Form::textarea('extract', null, ['class' => 'form-control', 'placeholder' => 'Escribe un pequeno resumen de tu experiencia ej: Software Developer en PHP etc ....']) !!}
</div>

<div class="form-group">
    {!! Form::label('title', 'Titulo') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Escribe el Nombre de la Empresa o Proyecto'] ) !!}
</div>

<div class="form-group">
    {!! Form::label('deateofork', 'Fecha de Empelo') !!}
    {!! Form::text('dateofwork', null , ['class' => 'form-control', 'placeholder' => 'Ej: Enero:2010 - Actual']) !!}
</div>

<div class="form-group">
    {!! Form::label('myactivities', 'Mis Funciones dentro de el Proyecto') !!}
    {!! Form::textarea('myactivities', null, ['class' => 'form-control', 'placeholder' => 'EJ: Como desarrollador de Software etc ...']) !!}
</div>

<div class="form-group">
    {!! Form::label('embbededvideo', 'Video Presentacion') !!}
    {!! Form::textarea('embbededvideo',null, ['class' => 'form-control', 'placeholder' => 'Embbeded Code Aqui','row' => '3']) !!}
</div>

<div class="form-group">
    {!! Form::submit('Guardar CV', ['class' => 'btn btn-success']) !!}
</div>



{!! Form::close() !!}