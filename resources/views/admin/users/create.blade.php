@extends('skeleton')


@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Welcome</h2>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Usuarios
                    </div>

                    <div class="panel-body">
                        <p>
                            <a href="{{ route('admin.users.index')}}" class="btn btn-info" role="button">Volver</a>
                        </p>
                        <hr>
                        {!! Form::open(['route' => 'admin.users.store', 'method' => 'POST']) !!}

                        @include('admin.users.components.errors')

                        @include('admin.users.components.forms')

                        <div class="from-group text-center">

                            {!! Form::submit('enviar', array(
                            'class' => 'btn btn-info btn-lg'
                            ))!!}
                        </div>




                        {!! Form::close() !!}


                    </div>
                </div>

            </div>
        </div>
    </div>

@stop


@section('scripts')
    @include('components.scripts')
@stop