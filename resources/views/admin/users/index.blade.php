@extends('skeleton')


@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop


@section('message')
    @include('flash::message')
@stop

@section('buscar')
         @include('components.usersearch')
@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">


            @include('admin.users.components.listado')

            </div>
        </div>
    </div>

    {!! Form::open(['route' => ['admin.users.destroy', ':USER_ID'], 'method' => 'DELETE', 'id' => 'form-delete' ]) !!}

    {!! Form::close() !!}


@stop


@section('scripts')
    @include('components.scripts')
@stop