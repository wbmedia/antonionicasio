<div class="panel panel-info">
    <div class="panel-heading">
        Usuarios
    </div>

    <div class="panel-body">
        <p>
            <a href="{{ route('admin.users.create')}}" class="btn btn-info" role="button">Crear Usuarios</a>
        </p>

        <table class="table table-hover table-striped">
            <tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Tipo</th>
                <th>Acciones</th>
            </tr>

            @foreach($users as $user)
                <tr data-id="{{ $user->id }}">
                    <th>{{$user->id}}</th>
                    <th>{{$user->first_name}}</th>
                    <th>{{$user->email}}</th>
                    <th>{{$user->type}}</th>
                    <th>
                        <a class="btn btn-warning" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil-square"></i></a>
                        <a class="btn btn-warning" href=""><i class="fa fa-eye"></i> </a>
                        <a class="btn btn-warning btn-delete-user" id="borrar" href="{{ route('admin.users.destroy', $user->id )}}"><i class="fa fa-trash"></i></a>
                    </th>
                </tr>
            @endforeach
        </table>

        {!! $users->render()  !!}

    </div>
</div>