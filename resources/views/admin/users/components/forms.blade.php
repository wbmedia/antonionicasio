<div class="from-group">
    {!! Form::label('first_name', 'Primer Nombre') !!}
    {!! Form::text('first_name', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe tu Primer Nombre'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('last_name', 'Apellido') !!}
    {!! Form::text('last_name', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe tu Apellido'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('username', 'Nombre de Usuario') !!}
    {!! Form::text('username', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Nombre de Usuario'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('email', 'Email') !!}
    {!! Form::email('email', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe tu  Email'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('phome', 'Telefono') !!}
    {!! Form::text('phone', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe tu  Telefono'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('website', 'Website') !!}
    {!! Form::text('website', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe tu  Sitio Web'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('github', 'Github') !!}
    {!! Form::text('github', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Github User'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('linkedin', 'Linkedin') !!}
    {!! Form::text('linkedin', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Linkedin URL'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('password', 'Password') !!}
    {!! Form::password('password',
    array(
    'class'=>'form-control',
    'placeholder' => 'Password'

    )) !!}
</div>

<div class="from-group">
    {!! Form::label('tipo', 'Tipo de Usuario') !!}
    {!! Form::select('type',[
    ''      => 'Seleccionar Tipo',
    'guest' => 'Guest',
    'user'  => 'User',
    'staff' => 'Staff',
    'admin' => 'Admin'
    ],null ,[
    'class' => 'form-control'
    ]) !!}
</div>

<div class="from-group">
    {!! Form::label('bio', 'Biografia') !!}
    {!! Form::textarea('bio', null,
    array(
    'class'=>'form-control',
    'placeholder' => 'Escribe un Extracto de tu Biografia'

    )) !!}
</div>
<br>
<hr>