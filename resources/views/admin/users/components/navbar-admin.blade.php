<nav class="navbar navbar-default ">
    <div class="container-fluid ">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>

        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav ">
                <li class="active"><a href="{{url('admin/home')}}">Home <span class="sr-only">(current)</span></a>
                <li><a href="{{url('admin/users')}}">Users</a></li>
                <li><a href="{{url('admin/categories')}}">Categories</a></li>
                <li><a href="{{url('admin/blog')}}">Blog News</a></li>
                <li><a href="{{url('admin/tags')}}">Tags</a></li>
                <li><a href="{{url('admin/images')}}">Images</a></li>
                <li><a href="{{url('admin/profile')}}">Profile</a></li>
                <li><a href="{{url('admin/resume')}}">CV</a></li>
                <li><a href="{{url('admin/portfolio')}}">Portfolio</a></li>
                <li><a href="{{url('admin/contacto')}}">Contacto</a></li>


                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Mi Cuenta <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#">Action</a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="#">Separated link</a></li>
                        <li class="divider"></li>
                        <li><a href="#">One more separated link</a></li>
                    </ul>
                </li>
                <li><a href="{{url('admin/settings')}}">Settings</a></li>
                <li> <a href="{{url('auth/logout')}}" class="">Salir</a></li>
            </ul>


        </div>
    </div>
</nav>