{!! Form::open(['route' => 'admin.users.index', 'method' => 'GET', 'class' => 'navbar-form ']) !!}
    <div class="form-group">
        {!! Form::text('name', null, ['class' => 'form-control','placeholder' =>  'Buscar Usuario'] ) !!}

        {!! Form::submit('Buscar', array(
        'class' => 'btn btn-info'
        )) !!}
    </div>


{!! Form::close() !!}