@extends('skeleton')


@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Welcome</h2>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        Editar Usuario : <h4>{{ $users->first_name }}</h4>
                    </div>

                    <div class="panel-body">
                        <p>
                            <a href="{{ route('admin.users.index')}}" class="btn btn-info" role="button">Volver</a>
                            @include('admin.users.components.borrar')
                        </p>
                        <hr>
                        {!! Form::model($users,['route' => ['admin.users.update', $users->id], 'method' => 'PUT']) !!}

                        @include('admin.users.components.errors')
                        @include('admin.users.components.forms')

                        <div class="from-group text-center">

                            {!! Form::submit('enviar', array(
                            'class' => 'btn btn-info btn-lg'
                            ))!!}
                        </div>




                        {!! Form::close() !!}





                    </div>
                </div>



            </div>
        </div>
    </div>

@stop


@section('scripts')
    @include('components.scripts')
@stop