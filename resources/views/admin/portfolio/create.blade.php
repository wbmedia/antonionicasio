@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop



@section('messages')


@stop



@section('contenido')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    Crear Proyecto Portfolio
                </div>
                <div class="panel-body">
                    {!! Form::open(['route' => 'admin.portfolio.store', 'method' => 'POST', 'files' => true ]) !!}

                    <div class="form-group">
                        {!! Form::label('name', 'Nombre de el Proyecto') !!}
                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Ingresa el Nombre de el Proyecto']) !!}

                    </div>

                    <div class="form-group">
                        {!! Form::label('tags', 'Tags') !!}
                        {!! Form::select('tags[]',$tags, null, [ 'class' => 'form-control tag-select','multiple', 'required'])!!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('name', 'Cargar Imagen') !!}
                        {!! Form::file('image', array('class' => 'btn btn-warning cargar-imagen', )) !!}
                    </div>

                    <div class="form-group">
                        {!! Form::label('info', 'info') !!}
                        {!! Form::textarea('info', null,array(
                        'class' => 'form-control blog-editor',

                        )) !!}
                    </div>

                    <div class="form-group text-center">
                        {!! Form::submit('Crear Portfolio', ['class' => 'btn btn-success']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('scripts')
    @include('components.scripts')
@stop

@section('js')


    <script>
        $('.tag-select').chosen({
            placeholder_text_multiple: 'Selecciona un maximo de 5 Tags',
            max_selected_options:'5',
            no_results_text: "No existen criterios de busqueda para tu seleccion",
        })

        $('.blog-editor').trumbowyg();
    </script>


@stop