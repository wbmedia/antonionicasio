



@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')

@stop

@section('messages')
    @include('flash::message')

@stop

@section('buscar')
    @include('components.blogsearch')
@stop

@section('contenido')


    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <a href="{{url('admin/portfolio/create')}}" class="btn btn-info">Anadir Portfolio</a>
                <hr>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Articulos
                    </div>
                    <div class="panel-body">

                        <div class="row">
                                @foreach($portfolio as $work)
                                <div class="col-xs-6 col-md-3">
                                    <a href="#" class="thumbnail">
                                        <img class="image-responsive thumbnail-imagen" src="/uploads/{{ $work->name }}" alt="{{ $work->name }}" >
                                    </a>
                                </div>
                                @endforeach
                        </div>



                    </div>
                </div>

                {!! $portfolio->render() !!}
            </div>
        </div>
    </div>

@stop

@section('scripts')
    @include('components.scripts')
@stop