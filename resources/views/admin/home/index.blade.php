@extends('skeleton')

@section('head')
    @include('components.head')
@endsection



@section('top')
    @include('components.top')
@stop

@section('buscar')
    @include('components.search')
@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-success">
                        <div class="panel-heading">Ultimos Usuarios</div>
                        <div class="panel-body">
                           <table class="table">
                               <tr>
                                   <th>Nombre </th>
                                   <th>Email</th>
                                   <th>Tipo</th>
                                   <th>Acciones</th>
                               </tr>


                               @foreach($users as $user)

                            <tr>

                                <td>{{$user->username}}</td>
                                <td>{{$user->email}}</td>
                                <td><botton class="btn btn-primary">{{$user->type}}</botton></td>
                                <td>
                                    <a class="btn btn-warning" href="{{ route('admin.users.edit', $user->id) }}"><i class="fa fa-pencil-square"></i></a>
                                </td>

                            </tr>
                               @endforeach

                           </table>


                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-info">
                        <div class="panel-heading">Ultimas Categorias</div>
                        <div class="panel-body">
                            <table class="table">
                                <tr>
                                    <th>Nombre de la Categoria</th>
                                    <th>Acciones</th>

                                </tr>



                                @foreach($categories as $category)
                                <tr>

                                    <td>{{$category->name}}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('admin.categories.edit', $category->id) }}"><i class="fa fa-pencil-square"></i></a>

                                    </td>


                                </tr>
                                @endforeach

                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Ultimas Entradas de el Blog</div>
                        <div class="panel-body">
                            <table class="table table-hover">
                                <tr>
                                    <th>Titulo</th>
                                    <th>Contenido</th>
                                    <th>Acciones</th>
                                </tr>

                                @foreach($blog as $article)
                                <tr>
                                    <td>{{$article->title}}</td>
                                    <td>{{$article->content}}</td>
                                    <td>
                                        <a class="btn btn-warning" href="{{ route('admin.blog.edit', $article->id) }}"><i class="fa fa-pencil-square"></i></a>

                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-warning">
                        <div class="panel-heading">Ultimos Mensajes</div>
                        <div class="panel-body">
                            <h2>Ultimos Mensajes</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection



@section('scrtips')
    @include('components.scripts')
@endsection