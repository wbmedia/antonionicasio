@extends('skeleton')

@section('head')
    @include('components.head')
@endsection



@section('top')
    @include('components.top')
@stop

@section('message')
    @include('flash::message')
@stop

@section('buscar')
    @include('components.categoriessearch')
@stop


@section('contenido')

    <div class="modal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&close;</button>
                    <h4 class="modal-title">Modal title</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'admin.categories.store', 'method' => 'POST']) !!}

                    @include('admin.users.components.errors')

                    @include('admin.categories.components.form-create')




                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Category</button>

                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

        <!--Start Test Categories -->



@include('admin.categories.components.table')

@endsection

@section('scripts')
    @include('components.scripts')
@endsection