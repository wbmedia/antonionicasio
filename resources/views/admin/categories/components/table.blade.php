<div class="container">
    <div class="row">
        <div class="panel panel-info">
            <div class="panel-heading">
                Categorias
            </div>

            <div class="panel-body ">
                <p>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target=".modal">Crear Categoria</button>

                </p>

                <table class="table table-hover table-striped">
                    <tr>
                        <th>#</th>
                        <th>Nombre de la Categoria</th>
                        <th>Creado</th>
                        <th>Editado</th>
                        <th>Acciones</th>
                    </tr>

                    @foreach($categories as $category)
                        <tr data-id="{{ $category->id }}">
                            <th>{{$category->id}}</th>
                            <th>{{$category->name}}</th>
                            <th>{{$category->created_at}}</th>
                            <th>{{$category->updated_at}}</th>

                            <th>
                                <a class="btn btn-warning" href="{{ route('admin.categories.edit', $category->id) }}"><i class="fa fa-pencil-square"></i></a>
                                <a class="btn btn-warning" href=""><i class="fa fa-eye"></i> </a>
                                <a class="btn btn-warning btn-delete-user" id="borrar"  href="{{ route( 'admin.categories.destroy', $category->id ) }}"><i class="fa fa-trash"></i></a>
                            </th>
                        </tr>
                    @endforeach
                </table>

                {!! $categories->render()  !!}

            </div>
        </div>
    </div>
</div>