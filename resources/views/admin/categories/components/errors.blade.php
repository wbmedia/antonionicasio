
@if ($errors->any())
    <div class="alert alert-danger" role="alert">
        <p>Upss! Algo anda mal Revisa los siguientes Errores</p>

        <ul>
            @foreach($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach
        </ul>

    </div>

@endif
