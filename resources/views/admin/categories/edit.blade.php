@extends('skeleton')


@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')
@stop

@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Welcome</h2>



                <div class="panel panel-default">
                    <div class="panel-heading">
                        Categorias
                    </div>

                    <div class="panel-body">
                        <p>
                            <a href="{{ route('admin.users.index')}}" class="btn btn-info" role="button">Volver</a>
                            @include('admin.categories.components.delete')
                        </p>
                        <hr>
                        {!! Form::open(['route' => ['admin.categories.update', $categories->id], 'method' => 'PUT']) !!}

                        @include('admin.categories.components.errors')

                        @include('admin.categories.components.form-edit')

                        <div class="form-group">
                            {!! Form::submit('Actualizar',array('class' => 'btn btn-info btn-lg'))!!}
                        </div>




                        {!! Form::close() !!}


                    </div>
                </div>

            </div>
        </div>
    </div>

@stop


@section('scripts')
    @include('components.scripts')
@stop