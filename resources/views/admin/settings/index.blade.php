



@extends('skeleton')

@section('head')
    @include('components.head')
@stop

@section('top')
    @include('components.top')

@stop

@section('messages')
    @include('flash::message')

@stop

@section('buscar')
    @include('components.blogsearch')
@stop

@section('contenido')

   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <div class="container">
                   <h2>
                     &nbsp; Website Settigns
                   </h2>
                   <ul class="nav nav-tabs" id="myTab">
                       <li class="active">
                           <a href="#home">Settings</a>
                       </li>
                       <li>
                           <a href="#profile">Edit Settings</a>
                       </li>
                       <li>
                           <a href="#e-settings">Email Setup</a>
                       </li>
                       <li>
                           <a href="#messages">Messages</a>
                       </li>
                   </ul>
                   <div class="tab-content">
                       <div class="tab-pane active" id="home">
                           <div class="col-md-12">


                               @foreach($settings as $settings)
                                   {!! Form::open() !!}
                                   {!! Form::label('websitetitle', 'Website Title') !!}
                                   {!! Form::text('websitetitle', $settings->websitetitle, ['class' => 'form-control', 'readonly']) !!}

                                   {!! Form::label('facebook', 'Facebook Icon Link') !!}
                                   {!! Form::text('facebook', $settings->facebook, ['class' => 'form-control', 'readonly']) !!}

                                   {!! Form::label('github', 'Github Icon Link') !!}
                                   {!! Form::text('github', $settings->github, ['class' => 'form-control', 'readonly']) !!}

                                   {!! Form::label('Twitter', 'Twitter Icon Link') !!}
                                   {!! Form::text('twitter', $settings->twitter, ['class' => 'form-control', 'readonly']) !!}

                                   {!! Form::label('linkedin', 'Linkedin Icon Link') !!}
                                   {!! Form::text('footer', $settings->linkedin, ['class' => 'form-control', 'readonly']) !!}


                                   {!! Form::label('footer', 'Footer Content') !!}
                                   {!! Form::text('footer', $settings->footer, ['class' => 'form-control', 'readonly']) !!}

                                   {!! Form::close() !!}

                               @endforeach

                           </div>
                       </div>
                       <div class="tab-pane" id="profile">
                           <div class="col-md-12">

                           </div>
                       </div>

                       <div class="tab-pane" id="e-settings">
                           <div class="col-md-12">
                               Email Setup
                           </div>
                       </div>

                       <div class="tab-pane" id="messages">
                           <div class="col-md-12">
                               Messeages
                           </div>
                       </div>
                   </div>
               </div>

           </div>
       </div>
   </div>



@stop

@section('scripts')
    @include('components.scripts')
@stop