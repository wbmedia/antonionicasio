@extends('skeleton')

@section('head')
    @include('components.head')
@endsection



@section('top')
    @include('components.top')
@stop

@section('messages')
    @include('components.errors')
    @include('flash::message')
@stop

@section('buscar')
    @include('components.search')
@stop

@section('contenido')
   <div class="container">
       <div class="row">
           <div class="col-md-12">
               <a href="{{url('admin/tags/create')}}" class="btn btn-info">Crear Tag</a>
               <hr>
               <div class="panel panel-info">
                   <div class="panel-heading">
                       Tags
                   </div>
                   <div class="panel-body">
                       <table class="table table-hover">
                           <tr>
                               <th>#</th>
                               <th>Nombre de el Tag</th>
                               <th>Creado</th>
                               <th>Editado</th>
                               <th>Acciones</th>
                           </tr>
                           @foreach($tags as $tag)
                           <tr>
                                <td>{{$tag->id}}</td>
                               <td>{{$tag->name}}</td>
                               <td>{{$tag->created_at}}</td>
                               <td>{{$tag->updated_at}}</td>
                                   <td>
                                       <a class="btn btn-warning" href="{{ route ('admin.tags.edit', $tag->id)}}">
                                           <i class="glyphicon glyphicon-eye-open"></i>
                                       </a>
                                       <a class="btn btn-warning" href="">
                                           <i class="glyphicon glyphicon-edit"></i>
                                       </a>
                                       <a class="btn btn-warning btn-delete-user" id="borrar" href="{{ route('admin.tags.destroy', $tag->id )}}"><i class="fa fa-trash"></i></a>
                                   </td>

                           </tr>
                           @endforeach
                       </table>
                   </div>

               </div>
               {!! $tags->render() !!}
           </div>
       </div>
   </div>



@endsection



@section('scrtips')
    @include('components.scripts')
@endsection