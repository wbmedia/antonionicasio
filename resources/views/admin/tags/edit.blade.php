@extends('skeleton')

@section('head')
    @include('components.head')
@endsection



@section('top')
    @include('components.top')
@stop

@section('messages')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="alert alert-dismissible alert-danger">
                    <button type="button" class="close" data-dismiss="alert">&close;</button>
                    @include('components.errors')
                </div>
            </div>
        </div>
    </div>
@stop


@section('contenido')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        Crear Tags
                    </div>
                    <div class="panel-body">
                        <p>
                            <a href="{{ route('admin.tags.index')}}" class="btn btn-info" role="button">Volver</a>
                            @include('admin.tags.components.borrar')
                        </p>
                        {!! Form::open(['route' => 'admin.tags.store', 'method' => 'POST']) !!}

                        <div class="form-group">
                            {!! Form::label('name', 'Nombre') !!}
                            {!! Form::text('name', $tags->name, array(
                            'class' => 'form-control',
                            'placeholder' => 'Ingresa El Nombre de el Tag'
                            )) !!}
                        </div>

                        <form-group>
                            {!! Form::submit('Crear Tag', array(
                            'class' => 'btn btn-success'
                            )) !!}
                        </form-group>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection



@section('scrtips')
    @include('components.scripts')
@endsection