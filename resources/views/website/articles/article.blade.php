@extends('website')

@section('head')
    @include('components-front.head')
@endsection

@section('header')
    @include('components-front.header')
@endsection

@section('contenido')
    <section class="content">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 no-padding">
                    <div class="col-md-12">
                        @foreach($blog as $article)
                            <article class="single-post">
                                <div class="post-header text-center">
                                    <h2><a href="#">{{ $article->title }}</a></h2>
                                    <span>Written by <a href="#">John Doe</a> in <a href="#">Design</a> on October 12, 2015 </span>
                                </div>
                                <div class="post-content">

                                    <a href="#"><img src="/uploads/feature-js.png" alt="" class="img-responsive"></a>

                                    {{ $article->content }}

                                </div>
                            </article>
                        @endforeach










                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection



@section('js')
    @include('components-front.js')
@endsection