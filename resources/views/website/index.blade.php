@extends('website')

@section('head')
    @include('components-front.head')
@endsection

@section('header')
    @include('components-front.header')
@endsection

@section('contenido')
        <!-- MAIN CONTENT -->
<div class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <div class="form-group">
                   <a href="{{url('admin/home')}}" class="btn btn-success">Login</a>
               </div>

                {!! Form::open() !!}
                <div class="form-group">
                    {!! Form::text('search', null, ['class' => 'form-control', 'placeholder' => 'Bucar en el Blog']) !!}

                </div>

                {!! Form::close() !!}
            </div>
        </div>
    </div>
    <br>
    <div class="container no-padding">


        <div class="row">
            <div class="col-md-3 l-content">
                <div class="profile-pic">
                    <div class="pic-bg"><img src="uploads/imagen_1455997596.jpg" class="img-responsive" alt=""/></div>
                </div>
                <nav>
                    <ul class="navigation">
                        <li><a href="#">Profile <i class="fa fa-user"></i></a></li>
                        <li><a href="#">Work <i class="fa fa-briefcase"></i></a></li>
                        <li><a href="#">Resume <i class="fa fa-file-text"></i></a></li>
                        <li><a href="#">Blog <i class="fa fa-comment"></i></a></li>
                        <li><a href="#">Contact <i class="fa fa-envelope"></i></a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-md-9 r-content">
                <div class="flexslider">
                    <div class="slides">
                        <!-- SECTION 1 - HOMEPAGE -->
                        <section class="no-display">
                            <div class="profile" id="1">
                                @foreach($users as $user)
                                <h2>Hello, I am <span>{!! $user->first_name !!}</span><br>{!! $user->bio !!}</h2>
                                <div class="sep1"></div>
                                <p>I'm a Software Developer. I have Experience as a Web & Software Developer. I Love and Passionete for  Software Development Coding in Laravel</p>
                                <div class="personal-info col-md-12 no-padding">
                                    <h4>Personal Info</h4>
                                    <div class="sep2"></div>
                                    <ul>
                                        <li>
                                            <div class="p-info"><em>name</em><span>{!! $user->first_name . '&nbsp' . $user->last_name !!}</span></div>
                                        </li>
                                        <li>
                                            <div class="p-info"><em>github</em><span>{!! $user->github !!}</span></div>
                                        </li>
                                        <li>
                                            <div class="p-info"><em>e-mail</em><span>{!! $user->email !!}</span></div>
                                        </li>
                                        <li>
                                            <div class="p-info"><em>website</em><span>{!! $user->website !!}</span></div>
                                        </li>
                                        <li>
                                            <div class="p-info"><em>phone</em><span>{!! $user->phone !!}</span></div>
                                        </li>
                                        <li>
                                            <div class="p-info"><em>linkedin</em><span>{!! $user->linkedin !!}</span></div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            @endforeach
                        </section>
                        <!-- SECTION 1 - HOMEPAGE -->

                        <!-- SECTION 2 - WORKS / PROJECTS / PORTFOLIO -->
                        <section class="no-display">
                            <div class="works" id="2">
                                <div class="page-head">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h3>Portfolio</h3>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="np-main">
                                                <p>Go to next/previous page</p>
                                                <div class="np-controls">
                                                    <a href="#" class="previous"><i class="fa fa-arrow-circle-left"></i></a>
                                                    <a href="#" class="next"><i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="portfolio-wrap">
                                    <div class="row">
                                        <ul id="filter-list" class="clearfix">
                                            <li class="filter" data-filter="all"><i class="fa fa-th"></i> All</li>
                                            <li class="filter" data-filter="webdesign">Web Design</li>
                                            <li class="filter" data-filter="appicon">App Icons</li>
                                            <li class="filter" data-filter="iosappui">iOS App UI</li>
                                            <li class="filter" data-filter="illustration">Illustration</li>
                                        </ul>
                                        <ul id="portfolio">
                                            <li class="item col-md-4 webdesign">
                                                <a title="Example 1" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/1.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 illustration">
                                                <a title="Sample 2" href="https://www.youtube.com/watch?v=L9szn1QQfas" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/2.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 appicon">
                                                <a title="i'm Number 3" href="http://vimeo.com/71071717" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/3.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 iosappui">
                                                <a title="Example 4" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/4.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 iosappui">
                                                <a title="Sample 5" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/5.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 illustration">
                                                <a title="Lorem ipsum 6" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/6.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 webdesign">
                                                <a title="Example 7" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/7.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 iosappui">
                                                <a title="Sample 8" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/8.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 webdesign">
                                                <a title="Project 9" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/9.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 illustration">
                                                <a title="Example 10" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/10.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 appicon">
                                                <a title="Item 11" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/11.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                            <li class="item col-md-4 iosappui">
                                                <a title="Sample 12" href="images/projects/1-big.jpg" data-lightbox-gallery="gallery1" class="nivo-lbox">
                                                    <div class="folio-img">
                                                        <img src="images/projects/12.jpg" alt="" class="img-responsive">
                                                        <div class="overlay">
                                                            <div class="overlay-inner">
                                                                <h4>Cool App Design</h4>
                                                                <p>branding, logo</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- SECTION 2 - WORKS / PROJECTS / PORTFOLIO -->

                        <!-- SECTION 3 - CV / RESUME -->
                        <section class="no-display">
                            <div class="item resume" id="3">
                                <div class="page-head">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h3>Resume</h3>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="np-main">
                                                <p>Go to next/previous page</p>
                                                <div class="np-controls">
                                                    <a href="#" class="previous"><i class="fa fa-arrow-circle-left"></i></a>
                                                    <a href="#" class="next"><i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="resume-info">
                                    <h4>Education</h4>
                                    <div class="sep2"></div>
                                    <ul>
                                        <li>
                                            <h5>Academy of Art University</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2001 - 2004</span>
                                            <p>Academy of Art University's School of Web Design & New Media is the intersection between traditional design and new technologies.</p>
                                        </li>
                                        <li>
                                            <h5>IT Technical Institute</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2005 - 2008</span>
                                            <p>Information technology (IT) workers can be found in many types of organizations. According to the U.S. Department of Labor, "In the modern workplace, it is imperative that Information Technology (IT) works both effectively and reliably</p>
                                        </li>
                                        <li>
                                            <h5>Web Design School</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2009 - 2012</span>
                                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat.</p>
                                        </li>
                                    </ul>
                                </div>
                                <div class="skills-info">
                                    <h4>Skills</h4>
                                    <div class="sep2"></div>
                                    <ul>
                                        <li>
                                            <p>Wordpress Development <span>71%</span></p>
                                            <div class="skills-bg"><span class="skill1"></span></div>
                                        </li>
                                        <li>
                                            <p>Photoshop <span>85%</span></p>
                                            <div class="skills-bg"><span class="skill2"></span></div>
                                        </li>
                                        <li>
                                            <p>HTML5/CSS3 <span>76%</span></p>
                                            <div class="skills-bg"><span class="skill3"></span></div>
                                        </li>
                                        <li>
                                            <p>Ruby on Rails <span>53%</span></p>
                                            <div class="skills-bg"><span class="skill4"></span></div>
                                        </li>
                                        <li>
                                            <p>Social Marketing <span>69%</span></p>
                                            <div class="skills-bg"><span class="skill5"></span></div>
                                        </li>
                                    </ul>
                                </div>
                                <div class="services-info">
                                    <h4>Services</h4>
                                    <div class="sep2"></div>
                                    <ul>
                                        <li>
                                            <i class="fa fa-cloud"></i>
                                            <h5>Design</h5>
                                        </li>
                                        <li>
                                            <i class="fa fa-smile-o"></i>
                                            <h5>Coding</h5>
                                        </li>
                                        <li>
                                            <i class="fa fa-desktop"></i>
                                            <h5>Responsive</h5>
                                        </li>
                                        <li>
                                            <i class="fa fa-text-width"></i>
                                            <h5>Planing</h5>
                                        </li>
                                        <li>
                                            <i class="fa fa-comment"></i>
                                            <h5>Wordpress</h5>
                                        </li>
                                        <li>
                                            <i class="fa fa-picture-o"></i>
                                            <h5>Photography</h5>
                                        </li>
                                    </ul>
                                </div>
                                <div class="resume-info">
                                    <h4>Work Experience</h4>
                                    <div class="sep2"></div>
                                    <ul>
                                        <li>
                                            <h5>Graphic River</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2001 - 2004</span>
                                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p>
                                        </li>
                                        <li>
                                            <h5>Video Hive</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2005 - 2008</span>
                                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit.</p>
                                        </li>
                                        <li>
                                            <h5>Themeforest</h5>
                                            <span class="year"><i class="fa fa-calendar"></i> 2009 - 2014</span>
                                            <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </section>
                        <!-- SECTION 3 - CV / RESUME -->

                        <!-- SECTION 4 - BLOG / NEWS -->
                        <section class="no-display">
                            <div class="item blog" id="4">
                                <div class="page-head">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h3>Blog</h3>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="np-main">
                                                <p>Go to next/previous page</p>
                                                <div class="np-controls">
                                                    <a href="#" class="previous"><i class="fa fa-arrow-circle-left"></i></a>
                                                    <a href="#" class="next"><i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="blog-wrap">
                                    @foreach($blog as $article)
                                    <article>
                                        <div class="row">
                                            <div class="col-md-5">
                                                <img src="uploads/imagen_1456122209.png" class="img-responsive" alt=""/>
                                            </div>
                                            <div class="col-md-7">
                                                <h3><a href="./blog-single.html">{{ $article->title }}</a></h3>
                                                <div class="post-meta">
                                                    <i class="fa fa-calendar"></i> <a href="#">{{ $article->created_at->diffForHumans() }}</a>
                                                    <i class="fa fa-user"></i> <a href="#">{{ $article->user->username }}</a>
                                                    <i class="fa fa-comments"></i> <a href="#">10 Comments</a>
                                                </div>
                                                <p> {{ $article->content }} </p>
                                            </div>
                                        </div>
                                    </article>
                                    @endforeach
                                    <div class="link-post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <i class="fa fa-link"></i>
                                                <a href="http://www.themeforest.net" target="_blank">www.themeforest.net</a>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="quote-post">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <blockquote>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aene.This is photoshop's version f Lorem Ipsum.</blockquote>
                                                <i class="fa fa-quote-left"></i>
                                                <span class="author-name">John Smith</span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </section>
                        <!-- SECTION 4 - BLOG / NEWS -->

                        <!-- SECTION 5 - CONTACT -->
                        <section class="no-height">
                            <div class="item contact" id="5">
                                <div class="page-head">
                                    <div class="row">
                                        <div class="col-md-5">
                                            <h3>Contact Us</h3>
                                        </div>
                                        <div class="col-md-7">
                                            <div class="np-main">
                                                <p>Go to next/previous page</p>
                                                <div class="np-controls">
                                                    <a href="#" class="previous"><i class="fa fa-arrow-circle-left"></i></a>
                                                    <a href="#" class="next"><i class="fa fa-arrow-circle-right"></i></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="map">
                                    <div class="gmap">
                                        <div id="map"></div>
                                    </div>
                                </div>
                                <div class="contact-info">
                                    <h4>Contact info</h4>
                                    <p>This is Photoshop's version  of Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio tincidunt auctor a ornare odio.</p>
                                    <ul>
                                        <li><i class="fa fa-home"></i> lorem ipsum street</li>
                                        <li><i class="fa fa-phone"></i> +399 (500) 321 9548</li>
                                        <li><i class="fa fa-envelope"></i> info@domain.com</li>
                                    </ul>
                                </div>
                                <div class="contact-form">
                                    <h4>Send us a message</h4>
                                    <form id="contactForm" action="php/contact.php" method="post">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <input type="text" name="senderName" id="senderName" placeholder="name">
                                                <input type="email" name="senderEmail" id="senderEmail" placeholder="e-mail">
                                                <input type="text" name="subject" id="subject" placeholder="Subject">
                                            </div>
                                            <div class="col-md-8">
                                                <textarea name="message" id="message" rows="6" placeholder="Message"></textarea>
                                                <button type="submit">Send Message</button>
                                            </div>
                                        </div>
                                    </form>
                                    <div id="successMessage" class="successmessage">
                                        <p><span class="success-ico"></span> Thanks for sending your message! We'll get back to you shortly.</p>
                                    </div>
                                    <div id="failureMessage" class="errormessage">
                                        <p><span class="error-ico"></span> There was a problem sending your message. Please try again.</p>
                                    </div>
                                    <div id="incompleteMessage" class="statusMessage">
                                        <p>Please complete all the fields in the form before sending.</p>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- SECTION 5 - CONTACT -->
                    </div>
                </div>

                <!-- FOOTER -->
                @foreach($settings as $footer)
                    <footer>
                        <div class="row">
                            <div class="col-md-7">
                                <p>{!! $footer->footer !!}</p>
                            </div>
                            <div class="col-md-5">
                                <ul class="social">
                                    <li><a href="https://www.facebook.com/profile.php?id=100000425302861"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://twitter.com/wbmediadigital"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="https://github.com/wbmedia"><i class="fa fa-github"></i></a></li>
                                    <li><a href="https://mx.linkedin.com/in/antonionicasio"><i class="fa fa-linkedin"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </footer>
                @endforeach
            </div>
        </div>
    </div>
</div>
<!-- MAIN CONTENT -->

@stop



@section('js')
    @include('components-front.js')
@endsection