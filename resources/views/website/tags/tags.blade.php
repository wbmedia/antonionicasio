<h3>Tag clouds</h3>
<nav>
    <ul class="tags list-inline">

        @foreach($tags as $tag)
            <li><a href="#">{{ $tag->name }}</a></li>
        @endforeach



    </ul>
</nav>