@extends('skeleton')


@section('head')
    @include('components.head')
@stop

@section('top')
    <div class="page-header text-center">
        <h1>Antonio Nicasio | WebDeveloper </h1>
    </div>


@stop

@section('contenido')
    <div class="conatiner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">Reset Password</div>
                    <div class="panel-body">
                        <div class="form-group">

                            <form method="POST" action="/password/email">
                                {!! csrf_field() !!}

                                @if (count($errors) > 0)
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                @endif

                                <div class="form-group">
                                    Email
                                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">
                                        Send Password Reset Link
                                    </button>

                                    <a class="btn btn-info" href="{{url('admin/home')}}">Volver</a>
                                </div>
                            </form>


                    </div>
                </div>
            </div>
        </div>
    </div>
@stop


@section('scripts')
    @include('components.scripts')
@stop