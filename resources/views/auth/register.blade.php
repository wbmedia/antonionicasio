@extends('skeleton')


@section('head')
@include('components.head')
@endsection

@section('top')
    <div class="page-header text-center">
        <h1>Antonio Nicasio | WebDeveloper </h1>
    </div>

    @include('admin.users.components.errors')
@endsection



@section('contenido')
     <div class="container">
         <div class="row">
             <div class="col-md-6 col-md-offset-3">

                 <div class="panel panel-info">
                     <div class="panel-heading ">Registro de Usuarios</div>
                     <div class="panel-body">

                         {!! Form::open(['route' => 'register.newuser.store', 'method' => 'POST']) !!}

                         <div class="form-group">
                             {!! Form::label('first_name', 'Nombre') !!}
                             {!! Form::text('first_name', null, array('class' => 'form-control', 'placeholder' => 'Ingresa tu Nombre')) !!}
                         </div>

                         <div class="form-group">
                             {!! Form::label('last_name', 'Apellido') !!}
                             {!! Form::text('last_name', null, array('class' => 'form-control', 'placeholder' => 'Ingresa tu Apellido')) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('username', 'Nombre de Usuario') !!}
                             {!! Form::text('username', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Nombre de Usuario'

                             )) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('email', 'Email') !!}
                             {!! Form::email('email', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Escribe tu  Email'

                             )) !!}
                         </div>
                         <div class="from-group">
                             {!! Form::label('phome', 'Telefono') !!}
                             {!! Form::text('phone', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Escribe tu  Telefono'

                             )) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('website', 'Website') !!}
                             {!! Form::text('website', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Escribe tu  Sitio Web'

                             )) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('github', 'Github') !!}
                             {!! Form::text('github', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Github User'

                             )) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('linkedin', 'Linkedin') !!}
                             {!! Form::text('linkedin', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Linkedin URL'

                             )) !!}
                         </div>


                         <div class="from-group">
                             {!! Form::label('password', 'Password') !!}
                             {!! Form::password('password',
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Password'

                             )) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('tipo', 'Tipo de Usuario') !!}
                             {!! Form::select('type',[
                             ''      => 'Seleccionar Tipo',
                             'user'  => 'User'

                             ],null ,[
                             'class' => 'form-control'
                             ]) !!}
                         </div>

                         <div class="from-group">
                             {!! Form::label('bio', 'Biografia') !!}
                             {!! Form::textarea('bio', null,
                             array(
                             'class'=>'form-control',
                             'placeholder' => 'Escribe un Extracto de tu Biografia'

                             )) !!}
                         </div>
                         <hr>
                         <div class="from-group text-center">

                             {!! Form::submit('Registrar', array(
                             'class' => 'btn btn-success '
                             ))!!}
                         </div>


                     </div>
                 </div>

             </div>
         </div>
     </div>

@endsection


@section('scripts')
    @include('components.scripts')
@endsection



