@extends('skeleton')


@section('head')
    @include('components.head')
@stop


@section('top')
    <div class="page-header text-center">
        <h1>Antonio Nicasio | WebDeveloper </h1>
        <div
                class="fb-like"
                data-share="true"
                data-width="450"
                data-show-faces="true">
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                @if ($errors->any())
                    <div class="alert alert-danger" role="alert">
                        <p>Upss! Algo anda mal Revisa los siguientes Errores</p>

                        <ul>
                            @foreach($errors->all() as $error)

                                <li>{{ $error }}</li>

                            @endforeach
                        </ul>

                    </div>

                @endif
            </div>
        </div>
    </div>

@stop

@section('contenido')

    <div class="conatiner">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-info">
                    <div class="panel-heading">Login</div>
                    <div class="panel-body">
                        <div class="form-group">

                            {!! Form::open() !!}
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">


                            {!! Form::label('email', 'Email:') !!}
                            {!! Form::email('email', null, array(
                            'class' => 'form-control',
                            'placeholder' => 'Email'
                            )) !!}
                        </div>

                        <div class="form-group">
                            {!! Form::label('password', 'Password:') !!}
                            {!! Form::password('password', array(
                            'class' => 'form-control',
                            'placeholder' => 'Password'
                            )) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Ingresar',array(
                            'class' => 'btn btn-success'
                            )) !!}
                            <br>
                            <a href="{{url('password/email')}}" class="pull-right">Olvidaste tu Contrasena</a>

                        </div>

                        {!! Form::close() !!}

                        <div class="form-group text-center">
                            <a class="btn btn-default" href="{{url('github/authorize')}}">Github Login</a>
                            <a class="btn btn-info" href="{{url('twitter/authorize')}}">Twitter Login</a>
                            <a class="btn btn-success" href="{{url('facebook/authorize')}}">Facebook Login</a>
                        </div>




                    </div>
                </div>
            </div>
        </div>
    </div>
    @stop

@section('scripts')
    @include('components.scripts')
@stop