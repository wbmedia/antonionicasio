@extends('skeleton')

@section('head')
    @include('components.head')
@stop


@section('top')
    <div class="page-header text-center">
        <h1>Antonio Nicasio | WebDeveloper </h1>
    </div>
@stop


@section('contenido')

<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <form method="POST" action="/password/reset">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                @if (count($errors) > 0)
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                <div class="form-group">
                    Email
                    <input class="form-control" type="email" name="email" value="{{ old('email') }}">
                </div>

                <div class="form-group">
                    Password
                    <input class="form-control" type="password" name="password">
                </div>

                <div class="form-group">
                    Confirm Password
                    <input class="form-control" type="password" name="password_confirmation">
                </div>

                <div class="form-group text-center">
                    <button class="btn btn-success" type="submit">
                        Reset Password
                    </button>

                    <a href="{{url('admin/home')}}" class="btn btn-info">Volver</a>
                </div>
            </form>
        </div>
    </div>
</div>

<hr>
@stop


@section('scripts')
   @include('components.scripts')
@endsection
