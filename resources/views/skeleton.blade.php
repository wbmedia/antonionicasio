<!doctype html>
<html lang="en">
<head>
@yield('head')
</head>
<body>

@yield('top')

@yield('messages')

@yield('navbar')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @yield('buscar')
        </div>
    </div>
</div>
@yield('contenido')


@yield('scripts')

@yield('js')
</body>
</html>