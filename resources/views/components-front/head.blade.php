<meta charset="UTF-8">
<!--Mobile Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- Meta -->

<meta name="description" content="Antonio Nicasio | Software & Web Developer">
<meta name="author" content="Antonio Nicasio">
<title>Antonio Nicasio  | Software Developer</title>

<!-- Favicons -->
<link rel="shortcut icon" href="img/favicon.ico">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">
<link rel="apple-touch-icon" sizes="144x144" href="img/apple-touch-icon-144x144.png">

<!-- Google Webfonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,700,800,700italic,600italic' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Neuton:400,200,300' rel='stylesheet' type='text/css'>

<!-- Bootstrap Core CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">

<!-- Stylesheets -->
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/nivo-lightbox.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/themes/default/default.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/hover-min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/css/main.css') }}">
<!-- Google fonts -->
<link href='https://fonts.googleapis.com/css?family=PT+Serif:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
<!-- modernizr -->
<script src="{{ asset('assets/js/modernizr.js') }}"></script>
<!--[if lt IE 9]>
  <script src="{{ asset('assets/js/html5shiv.js')}}"></script>
  <script src="{{ asset('assets/js/respond.min.js')}}"></script>
<![endif]-->

<!-- LOADING MASK -->
<!--<div id="mask">
  <div class="loader">
    <img src="images/loading.gif" alt='loading'>
  </div>
</div>-->