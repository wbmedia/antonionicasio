<!-- script tags
	============================================================= -->
<script src="{{ asset('assets/js/jquery-2.1.1.js')}}"></script>
<script src="{{ asset('assets/js/smoothscroll.js')}}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{ asset('assets/js/animations.js') }}"></script>
<script src="{{ asset('assets/js/chosen.jquery.js') }}"></script>
<script src="{{ asset('assets/js/chosen.proto.js') }}"></script>
<script src="{{ asset('assets/js/contact.js') }}"></script>
<script src="{{ asset('assets/js/html5shiv.js') }}"></script>
<script src="{{ asset('assets/js/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/js/jquery.easing.js') }}"></script>
<script src="{{ asset('assets/js/jquery.flexslider.js') }}">
<!--<script src="{{ asset('assets/js/inview.js') }}"></script-->
<script src="{{ asset('assets/js/jquery.mixitup.js') }}"></script>
<script src="{{ asset('assets/js/modernizr.js') }}"></script>
<script src="{{ asset('assets/js/nivo-lightbox.min.js') }}"></script>
<script src="{{ asset('assets/js/respond.min.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<script src="{{ asset('assets/js/gmaps.js') }}"></script>