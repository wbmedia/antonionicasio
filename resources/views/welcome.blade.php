<!DOCTYPE html>
<html>
    <head>
        <title>Laravel</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/lumen/bootstrap.min.css" rel="stylesheet" integrity="sha256-QSktus/KATft+5BD6tKvAwzSxP75hHX0SrIjYto471M= sha512-787L1W8XyGQkqtvQigyUGnPxsRudYU2fEunzUP5c59Z3m4pKl1YaBGTcdhfxOfBvqTmJFmb6GDgm0iQRVWOvLQ==" crossorigin="anonymous">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha256-3dkvEK0WLHRJ7/Csr0BZjAWxERc5WH7bdeUya2aXxdU= sha512-+L4yy6FRcDGbXJ9mPG8MT/3UCDzwR9gPeyFNMCtInsol++5m3bk2bXWKdZjvybmohrAsn3Ua5x8gfLnbE1YkOg==" crossorigin="anonymous">
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
                color: #ecf0f1;
                background: #2980b9 url('http://static.tumblr.com/03fbbc566b081016810402488936fbae/pqpk3dn/MRSmlzpj3/tumblr_static_bg3.png') repeat 0 0;
                -webkit-animation: 10s linear 0s normal none infinite animate;
                -moz-animation: 10s linear 0s normal none infinite animate;
                -ms-animation: 10s linear 0s normal none infinite animate;
                -o-animation: 10s linear 0s normal none infinite animate;
                animation: 10s linear 0s normal none infinite animate;

            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            
            h1 {
                color: #ecf0f1;
            } 


            @-webkit-keyframes animate {
                from {
                    background-position: 0 0;
                }
                to {
                    background-position: 500px 0;
                }
            }

            @-moz-keyframes animate {
                from {
                    background-position: 0 0;
                }
                to {
                    background-position: 500px 0;
                }
            }

            @-ms-keyframes animate {
            from {
                background-position: 0 0;
            }
            to {
                background-position: 500px 0;
            }
            }

            @-o-keyframes animate {
                from {
                    background-position: 0 0;
                }
                to {
                    background-position: 500px 0;
                }
            }

            @keyframes animate {
                from {
                    background-position: 0 0;
                }
                to {
                    background-position: 500px 0;
                }
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="content">
                        <div class="title">Antonio Nicasio</div>
                        <h1>Software Developer</h1>

                    </div>
                    <div class="form-group">
                        <a class="btn btn-success" href="{{url('/website')}}">Ingresar</a>
                    </div>
                    {!! Form::open(['method' => 'POST', 'class' => 'form-inline']) !!}
                    <div class="form-group">

                        {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Suscribete... Ingresa tu Email']) !!}
                        {!! Form::submit('Suscribirme', ['class' => 'btn btn-warning']) !!}

                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>


    </body>
</html>
