<!doctype html>
<html lang="en">
<head>
    @yield('head')
</head>
<body>

@yield('header')

@yield('contenido')


</body>

@yield('js')
</html>