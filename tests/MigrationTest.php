<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;



class MigrationTest extends TestCase
{
    use DatabaseMigrations;
    public function testBasicExample()
    {
        $this->visit('/')
        ->see('Antonio Nicasio');
    }
}
