<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username')->nullable();
            $table->string('avatar');
            $table->string('provider');
            $table->string('provider_id');
            $table->string('email')->unique()->nullable();
            $table->string('phone');
            $table->string('website');
            $table->string('github');
            $table->string('linkedin');
            $table->string('password', 60);
            $table->enum('type', ['guest'=>'guest', 'user' => 'user', 'staff' => 'staff', 'admin' => 'admin'])->default('user');
            $table->longText('bio');
            $table->string('confirm_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
