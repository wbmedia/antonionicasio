<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resume', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('position');
            $table->longText('extract');
            $table->string('title');
            $table->string('location');
            $table->string('dateofwork');
            $table->longText('myactivities');
            $table->longText('embbededvideo');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table::drop('resume');
    }
}
