<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Foundation\Auth\Access\Authorizable;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = ['name'];

    public function scopeSearch($query, $name)
    {
        return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeSearchCategory($query, $name )
    {
        return $query->where('name', 'LIKE', "%$name%");
    }
}
