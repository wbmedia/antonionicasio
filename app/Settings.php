<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $table = 'settings';
    protected $fillable = [ 'websitetitle', 'footer', 'facebook', 'guthub', 'linkedin', 'twitter'];
}
