<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tags;

class Portfolio extends Model
{
    protected $table = 'portfolio';
    protected $fillable = ['name', 'image' , 'info', 'tag_id'];

    public function tags()
    {
        return $this->belongsToMany('App\Tags');
    }
    public function images()
    {
        return $this->hasMany('App\Image');
    }
}
