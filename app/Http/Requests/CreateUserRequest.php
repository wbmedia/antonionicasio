<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3',
            'last_name'  => 'required|min:2',
            'username'   => 'required|min:4|unique:users',
            'email'      => 'required|unique:users',
            'phone'      =>  'required|unique:users',
            'github'     => 'unique:users',
            'linkedin'   => 'unique:users',
            'password'   => 'required|min:6',
            'bio'        => 'required',
            'type'       =>  'required|in:guest,user,staff,admin'
        ];
    }
}
