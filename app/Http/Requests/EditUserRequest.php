<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Routing\Route;

class EditUserRequest extends Request
{
    public function __construct(Route $route)
    {
        $this->route = $route;

    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|min:3',
            'last_name'  => 'required|min:2',
            'username'   => 'required|min:4, unique:users,username' . $this->route->getParameter('users') ,
            'email'      => 'required|unique:users,email,' . $this->route->getParameter('users'),
            'password'   => '',
            'bio'        => 'required',
            'type'      =>  'required'
        ];
    }
}
