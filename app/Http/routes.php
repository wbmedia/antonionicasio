<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});





// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register/', 'Auth\AuthController@getRegister');
Route::post('auth/register/', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// Github Login routes...
Route::get('auth/github', 'Auth\AuthController@login');
Route::get('auth/github/callback', 'Auth\AuthController@loginCallback');


// OAuth Github Login rotes..
Route::get('github/authorize', 'Auth\GithubController@githubAuthorize');

Route::get('github/login', 'Auth\GithubController@githubLogin');

// OAuth Facebook Login rotes..
Route::get('facebook/authorize', function(){
    return OAuth::authorize('facebook');
});

Route::get('facebook/login', function(){
    OAuth::login('facebook');
    return view('admin/home/index');
});

Route::get('sitio', function() {
    return view('website/home');
});
Route::get('website', 'Admin\WebsiteController@index');
Route::get('article', 'Admin\WebsiteController@getArticle');
Route::get('categories/{name}', [
   'uses' => 'Admin\WebsiteController@searchCategory',
    'as' => 'website.search.category'
]);

Route::get('tags/{name}', [
    'uses' => 'Admin\WebsiteController@searchTag',
    'as' => 'website.search.tag'
]);


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::get('home','Admin\HomeController@index');
    Route::resource( 'users', 'Admin\UsersController');
    Route::resource('categories','Admin\CategoriesController');
    Route::resource('blog','Admin\BlogController');
    Route::resource('profile','Admin\ProfileController');
    Route::resource('resume', 'Admin\ResumeController');
    Route::resource('tags', 'Admin\TagsController');
    Route::resource('resume', 'Admin\CvController');
    Route::resource('images', 'Admin\ImageController');
    Route::resource('portfolio', 'Admin\PortfolioController');
    Route::resource('settings', 'Admin\SettingsController');




});

Route::group(['prefix' => 'register'], function(){
    Route::resource( 'newuser', 'Auth\AuthController');
    Route::get('register/confirmation/{token}', 'Auth\AuthController@store');
});





