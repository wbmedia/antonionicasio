<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\User;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Routing;
use App\Http\Requests\EditUserRequest;
use App\Http\Requests\CreateUserRequest;

use Caffeinated\Flash\FlashHandler;
use Illuminate\Support\Facades\Session;

use Laracasts\Flash\Flash;




class UsersController extends Controller
{

    public function __construct(Request $request)
    {
        //$this->middleware('auth');
        $this->request = $request;

    }



    public function index(Request $request)
    {

        $users = User::search($request->name)->orderBy('id', 'ASC')
            ->paginate('15');
        return view('admin.users.index',compact('users'));
        //dd($users);
    }


    public function create()
    {
        return view('admin.users.create');
    }

    public function store(Requests\CreateUserRequest $request)
    {

        $users = new User($request->all());
        $users->save();

        return redirect('admin/home');
        //dd($users);

    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $users = User::findOrFail($id);

        return view('admin.users.edit', compact('users'));
    }


    public function update(Requests\EditUserRequest  $request, $id)
    {
        $users = User::findOrFail($id);

        $users->fill($request->all());
        $users->save();
        return redirect('admin/users');
    }


    public function destroy($id)
    {
        User::destroy($id);
        return redirect('admin/users');

    }
}
