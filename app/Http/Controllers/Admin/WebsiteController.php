<?php

namespace App\Http\Controllers\Admin;

use App\Blog;
use Illuminate\Http\Request;

use App\User;
use App\Category;
use App\CV;
use App\Settings;
use App\Image;
use App\Tags;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use phpDocumentor\Reflection\DocBlock\Tag;

class WebsiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        Carbon::setLocale('es');
    }

    public function index()
    {
        $blog = Blog::orderBy('id', 'DESC')->paginate('8');
        $blog->each(function($blog){
            $blog->category;
            $blog->user;

        });
        //dd($blog);

        $users = DB::table('users')->orderBy('id', 'ASC')->paginate(1);
        $settings = DB::table('settings')->orderBy('id', 'ASC')->paginate(1);
        //dd($users);
        $tags  = Tags::orderBy('id');
        $image = Image::orderBy('id');
        //dd($users, $image, $blog);
        return view('website.index', compact('blog', 'image', 'users', 'settings'));
    }



    public function getArticle()
    {
        $blog = Blog::where('id')->get();
        return view('website.articles.article',compact('blog'));
    }

    public function searchCategory($name)
    {
        $category = Category::SearchCategory($name)->get();
        dd($category);
    }

    public function searchTag($name)
    {
        $tags = Tag::SearchTag($name)->get();
        dd($tags);
    }
}
