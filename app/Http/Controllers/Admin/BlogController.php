<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Category;
use App\Blog;
use App\Tags;
use App\Image;

use Laracasts\Flash\Flash;
use phpDocumentor\Reflection\DocBlock\Tag;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $blog = Blog::search($request->name)->orderBY('id', 'ASC')->paginate(15);
        $blog->each(function($blog){
            $blog->category;
            $blog->user;
        });
        return view('admin.blog.index', compact('blog'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::orderBy('id', 'ASC')->lists('name', 'id');
        $tags = Tags::orderBy('id', 'ASC')->lists('name', 'id');
        //dd($categories, $tags);
        return view('admin.blog.create', compact('tags', 'categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreateBlogRequest $request)
    {


        $file = $request->file('image');
        $name = 'imagen_' . time() . '.' . $file->getClientOriginalExtension();
        $path = public_path() . '/uploads';
        $file->move($path, $name);


        $blog = new Blog($request->all());
        $blog->user_id = \Auth::user()->id;
        $blog->save();

        $blog->tags()->sync($request->tags);

        $image = new Image();
        $image->name = $name;
        $image->article()->associate($blog);
        $image->save();

        return redirect('admin/blog');







        //dd( $blog, $categories, $tags, $image, $file, $path, $name);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
