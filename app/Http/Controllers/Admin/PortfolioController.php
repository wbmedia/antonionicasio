<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Tags;
use App\Portfolio;
use App\Image;

class PortfolioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $portfolio = Portfolio::orderBy('id', 'ASC')->paginate('15');
        return view('admin.portfolio.index', compact('portfolio'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $tags = Tags::orderBy('id', 'ASC')->lists('name', 'id');
        return view('admin.portfolio.create', compact('tags', 'categories'));
       // dd($tags , $request);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\CreatePortfolioRequest $request)
    {
        $file = $request->file('image');
        $name = 'image_' . time() . '.' . $file->getClientOriginalExtension();
        $path = public_path() . '/portfolio';
        $file->move($path, $name);

        $portfolio = new Portfolio($request->all());
        $portfolio->tags()->sync($request->tags);

        $image = new Image();
        $image ->name = $name;
        $image ->portfolio()->associate($portfolio);
        $portfolio->save();

        //dd($portfolio, $image);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
