<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \OAuth;
use App\User;

class GithubController extends Controller
{

    public function githubAuthorize()
    {
        return OAuth::authorize('github');
    }

    public function githubLogin()
    {
        OAuth::login('github');
        //$user = Auth::User();
        return view ('admin/home/index');
    }


}
