<?php
namespace App\Http\ViewComposers;

use App\Tags;
use Illuminate\Contracts\View\View;
use App\Category;
use App\Blog;

  class AsideComposer
 {
      public function compose(View $view)
      {
          $categories = Category::orderBy('name', 'ASC')->get();
          $tags = Tags::orderBy('id', 'ASC')->get();
          $article = Blog::orderBY('id', 'ASC') ->take(5)->get();

          $view->with('categories', $categories)->with('tags', $tags)->with('article', $article);
      }

 }