<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'oauth_identities';
    protected  $fillable = ['user_id','provider_user_id', 'provider', 'access_token'];
}
