<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';
    protected $fillable = ['name'];

    public function articles()
    {
        return $this->belongsTo('App\Blog')->withTimestamps();
    }

    public function portfolio()
    {
        return $this->belongsTo('App\Porfolio')->withTimestamps();
    }

    public function scopeSearch($query, $name)
    {
        return $query->where('name', 'LIKE', "%$name%");
    }

    public function scopeSearchTag($query, $name )
    {
        return $query->where('name', 'LIKE', "%$name%");
    }
}
