<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CV extends Model
{
    protected $table = 'resume';

    protected $fillable = ['position', 'extract', 'title', 'location', 'dateofwork', 'miactivities', 'embbededvideo'];
}
